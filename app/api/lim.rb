require 'open-uri'
require 'nokogiri'

module LIM
  class API < Grape::API

    version 'v1', using: :header, vendor: 'lim'
    format :json
    formatter :json, Grape::Formatter::ActiveModelSerializers
    prefix :api

    rescue_from Grape::Exceptions::ValidationErrors do |e|
      error!({ errors: e.full_messages, details: e }, 400)
    end


    helpers do
      def warden
        env['warden']
      end

      def authenticated
        return true if warden.authenticated?

        uid = params['uid'] || request.headers['Uid']
        @user = User.find_by(uid: uid)
        access_token = params['access-token'] || request.headers['Access-Token']
        client = params['client'] || request.headers['Client']
        access_token && client && @user && @user.valid_token?(access_token, client)
      end

      def current_user
        warden.user || @user
      end

      def authenticate!
        error!({ errors: ['401 Unauthorized']}, 401) unless authenticated
      end

      def api_error!(errors = [], e)
        resp = { errors: errors}
        if Rails.env.development?
          resp[:details] = [e.message]
        end

        error!(resp, 400)
      end

    end


    # API

    resource :contents do
      desc 'do the page parsing according to provided URL'
      params do
        requires :url, type: String, regexp: /(^$)|(^(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(([0-9]{1,5})?\/.*)?$)/ix, desc: 'An URL to be parsed'
      end

      post do
        authenticate!

        begin
          extractor = Lim::ExtractorService.new({:url => params[:url], :tags => %w(h1 h2 h3)})
          # include_links = false
          # extractor.fetch(include_links) # without saving to db
          extractor.pull
        rescue Lim::ExtractorException => e
          api_error!(["Couldn't fetch from provided remote server", 'Please double-check provided URL'], e)
        end

      end


      desc 'list of all saved contents'
      params do
        optional :url, type: String, regexp: /https?:\/\/[\S]+/ , desc: 'An URL to filter the results'
      end
      paginate per_page: '10'

      get each_serializer: ResourceSerializer, root: :resources, adapter: :json do
        authenticate!

        if params.include? :url
          @resource = Resource.where(url: params[:url])
        else
          @resource = Resource.all
        end
        paginate @resource

      end

    end



  end
end