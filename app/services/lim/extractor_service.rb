require 'open-uri'
require 'nokogiri'

module Lim

  class ExtractorException < Exception
  end

  class ExtractorService

    attr_reader :url, :doc, :tags

    attr_accessor :contents, :links

    def initialize(params, preload = false)
      unless params.include?(:url)
        raise Lim::ExtractorException.new('No URL given')
      end

      @url = params[:url]

      if params.include?(:tags) && params[:tags].is_a?(Array)
        @tags = params[:tags]
      else
        @tags = %w(h1 h2 h3 h4 h5 h6) # default tags to be parsed
      end

      @contents = Hash.new
      @links = []

      doc if preload
    end

    # do the hard work :)
    def pull(include_links = true, save = true)

      needed_tags = @tags

      resource = Resource.where(url: @url).first_or_initialize if save

      needed_tags.each do |t|
        texts = []
        doc.css(t).each do |e|
          # Rails.logger.debug e
          if e.text.present? && e.text.length > 0
            content = e.text
            texts << content
            c = Content.where(tag: t, content: content).first_or_initialize if save
            resource.contents.push(c) if save
          end

        end
        @contents[t.to_sym] = texts if texts.count > 0
      end


      # if we need just a content of a tags, without links
      if include_links
        needed_tags = %w(a)

        needed_tags.each do |t|
          texts = []
          doc.css(t).each do |e|
            if e.attr('href').present? && e.attr('href').length > 1 # we don't want to include stuff like '#''
              content = e.attr('href')
              texts << content

              c = Content.where(tag: t, content: content).first_or_initialize if save
              resource.contents.push(c) if save
            end

          end
          @links << texts if texts.count > 0
        end
      end

      resource.save if save

      to_hash
    end

    # just to grab a contents without saving into database
    def fetch(include_links = true)
      pull(include_links, false)
    end

    def to_hash
      {:contents => contents, :links => links.present? && links.count > 0 ? links[0] : []}
    end

    def to_json
      to_hash.to_json
    end



    private

    def doc
      unless @doc
        begin
          @doc = Nokogiri::HTML(open(@url))
        rescue SocketError, OpenSSL::SSL::SSLError, RuntimeError => e
          Rails.logger.error e.message
          raise Lim::ExtractorException.new(e.message)
        end
      end
      @doc
    end

    def needed_tags
      @tags ||= []
    end

  end
end