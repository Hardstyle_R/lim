class ContentSerializer < ActiveModel::Serializer
  attributes :tag, :content
  has_one :resource
end