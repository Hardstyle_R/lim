class ResourceSerializer < ActiveModel::Serializer
  attributes :id, :url
  has_many :contents
  has_many :links

  def contents
    object.contents.select { |x|
      # %w(h1 h2 h3 h4 h5 h6).include? x.tag
      #   ActiveModel::Serializer::Adapter::JsonApi.new(
      #       ContentSerializer.new(x)).as_json
      # else
      #   ActiveModel::Serializer::Adapter::Null.new(x)
      # end

      %w(h1 h2 h3 h4 h5 h6).include? x.tag
    }
  end

  def links
    object.contents.select { |x|
      x.tag == 'a'
    }.map {|x|
      x.content
    }
  end

end