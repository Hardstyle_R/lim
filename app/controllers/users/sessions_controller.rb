#app/controllers/users/sessions_controller.rb
class Users::SessionsController < DeviseTokenAuth::SessionsController
  protect_from_forgery except: :create

  def create
    super
  end
end