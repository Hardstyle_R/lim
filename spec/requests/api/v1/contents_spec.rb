require 'rails_helper'

describe LIM::API do

  before do
    #we have to create a real user before a calls
    @user = User.new(email: 'roman.osidach@gmail.com', password: 'lim_password', :confirmed_at => DateTime.now)
    @user.skip_confirmation_notification!
    @user.save!

    post '/api/v1/auth/sign_in', {email: @user.email, password: @user.password}

    @access_token = response.headers['access-token']
    @client = response.headers['client']
    @uid = response.headers['uid']

  end
  it 'should sign in' do
    # @user = FactoryGirl.create(:user, email: 'roman.osidach@gmail.com', password: 'lim_password')

    post '/api/v1/auth/sign_in', {email: @user.email, password: @user.password}

    expect(response).to have_http_status(:success)


    expect(response.headers).to have_key('access-token')
    expect(response.headers).to have_key('uid')
    expect(response.headers).to have_key('client')

    expect(response.headers['access-token']).to be_an_instance_of(String)
    expect(response.headers['client']).to be_an_instance_of(String)
    expect(response.headers['token-type']).to eq('Bearer')
    expect(response.headers['uid']).to eq(@user.email)



  end

  it 'should process the URL' do
    auth_headers = {'Uid' => @uid, 'Access-Token' => @access_token, 'Client' => @client,
                    "Accept" => "application/json",
                    "Content-Type" => "application/json"}
    @request.headers.merge!(auth_headers)


    post '/api/contents?url=https://facebook.com', {}, auth_headers

    expect(response).to have_http_status(201)

    contents = JSON.parse(response.body)

    expect(contents).to have_key('contents')
    expect(contents).to have_key('links')
  end

  it 'should list contents' do
    get '/api/contents', {}, {'Uid' => @uid, 'Access-Token' => @access_token, 'Client' => @client,
                              "Accept" => "application/json",
                              "Content-Type" => "application/json"}


    expect(response).to have_http_status(:success)

    resources = JSON.parse(response.body)

    expect(resources).to have_key('resources')
    expect(resources['resources']).to be_an_instance_of(Array)
    expect(resources['resources'][0]).to have_key('url')
    expect(resources['resources'][0]).to have_key('contents')
    expect(resources['resources'][0]).to have_key('links')
    expect(resources['resources'][0]['contents']).to be_an_instance_of(Array)
    expect(resources['resources'][0]['links']).to be_an_instance_of(Array)
  end
end