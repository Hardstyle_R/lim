# LIM Content Extractor #

LIMCE is a tiny RESTful API to index a page's content

### Summary ###

LIMCE is a tool to simply index the content of a page. It actually parses the page content and stores its content (all that is found inside the tags h1, h2 and h3, and the links) at the database

LIMCE depends on few important gems:

* [Grape](https://github.com/ruby-grape/grape)
* [Devise](https://github.com/plataformatec/devise) & [devise_token_auth](https://github.com/lynndylanhurley/devise_token_auth)
* [Nokogiri](https://github.com/sparklemotion/nokogiri)

### Installation ###

Clone the project:

``git clone git@bitbucket.org:Hardstyle_R/lim.git``

Run ``bundle install``

You should have the PostgreSQL engine run on your machine. Edit your ``database.yml`` configuration accordingly (./config/database.yml)

Run ``rake db:migrate `` to have a database structure

Run ``rails c`` and create a user to be able to sign into the API and get an access token

Then run ``rails s`` and call the API endpoints described below.

To check if everything is well, run tests:
``rake test`` and ``rspec spec/requests/api/v1/contents_spec.rb``

### Accessing an API ###


#### Auth ####
Hit the Auth endpoint:

``curl --include -X POST http://localhost:3000/api/v1/auth/sign_in?email=roman.osidach@gmail.com&password=lim_password``

Copy keys from headers. They would look like:

````
access-token: sIDEss4Q46DCJ4eMkn1WbA
token-type: Bearer
client: Srox6CL1ItvyHgsQLrfrag
expiry: 1474555459
uid: roman.osidach@gmail.com
````
Then having pairs uid/access-token and the client you are able to access the LIMCE API:

* POST /api/contents?url=<YOUR_URL> - Post URL
* GET /api/contents?url=<YOUR_URL(OPTIONAL)> - Get Contents

#### Post URL ####
An endpoint receives the URL of the page as a parameter, grab the contents and stores it at the database

* the only contents of HTML header tags (H1, H2 and H3) are stored
* and links URLs also ('a' tag)
````
curl -X POST -H "uid: roman.osidach@gmail.com" -H "access-token: FukgtDEVqyHoCh_7u-rSHQ" -H "client: osstQh0K2pQK_kbYcKV82w" -H "Cache-Control: no-cache" "http://localhost:3000/api/contents?url=<YOUR_URL>"
````
#### Get Contents ####
An endpoint returns the list of grabbed contents previously accomplished from POST /api/contents by each resource (URL)

````
curl -X GET -H "uid: roman.osidach@gmail.com" -H "access-token: FukgtDEVqyHoCh_7u-rSHQ" -H "client: osstQh0K2pQK_kbYcKV82w" -H "Cache-Control: no-cache" "http://localhost:3000/api/contents?url=<YOUR_URL(OPTIONAL)>"
````

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Added some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request

## Maintainers

* Roman Osidach (https://bitbucket.org/Hardstyle_R)

## License

MIT License.