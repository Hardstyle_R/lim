class CreateContents < ActiveRecord::Migration
  def change
    create_table :contents do |t|
      t.string :tag
      t.string :content
      t.references :resource, index: true

      t.timestamps null: false
    end
    add_foreign_key :contents, :resources
  end
end
